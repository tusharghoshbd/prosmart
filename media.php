<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from profs.etsmtl.ca/mcheriet/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 May 2015 14:35:44 GMT -->
    <head>
        <title>Professor Mohamed Cheriet personal page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="sarah" content="">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


        <!--CSS styles-->
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">  
        <link rel="stylesheet" href="css/perfect-scrollbar-0.4.5.min.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/style.css">
        <link id="theme-style" rel="stylesheet" href="css/styles/cyan.css">


        <!--/CSS styles-->
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
        <script src="../../ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

        <script src="js/socialbars.js" type="text/javascript"></script>


        <link rel="stylesheet" type="text/css" href="css/social.css" />


        <!--Javascript files-->
        <link rel="stylesheet" type="text/css" href="css/citation.css" />

        <script type="text/javascript" src="js/TweenMax.min.js"></script>
        <script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
        <script type="text/javascript" src="js/jquery.carouFredSel-6.2.1-packed.js"></script>

        <script type="text/javascript" src="js/modernizr.custom.63321.js"></script>
        <script type="text/javascript" src="js/jquery.dropdownit.js"></script>

        <script type="text/javascript" src="js/jquery.stellar.min.js"></script>
        <script type="text/javascript" src="js/ScrollToPlugin.min.js"></script>

        <script type="text/javascript" src="js/bootstrap.min.js"></script>

        <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>

        <script type="text/javascript" src="js/masonry.min.js"></script>

        <script type="text/javascript" src="js/perfect-scrollbar-0.4.5.with-mousewheel.min.js"></script>

        <script type="text/javascript" src="js/magnific-popup.js"></script>
        <script type="text/javascript" src="js/custom.js"></script>

        <!--/Javascript files 
        <script type="text/javascript" src="sliderengine/jquery.js"></script>-->
        <script type="text/javascript" src="sliderengine/jquery.hislider.js"></script>

        <link rel='stylesheet' type='text/css' href='css/styles.css' />

        <script type='text/javascript' src='css/menu_jquery.js'></script>





    </head>
    <body >
        <div id="wrapper">
            <a href="#sidebar" class="mobilemenu"><i class="icon-reorder"></i></a>

            <div id="sidebar">
                <div id="main-nav">
                    <div id="nav-container">
                        <br/>
                        <!---<img src="img/ets.png" width="130" height="50"/></center> -->

                        <div id="profile" class="">
                            <div class="portrate hidden-xs"></div>
                            <div class="title">
                                <h2><p>Mohamed CHERIET</p><p> Eng., Ph.D., SMIEEE</p></h2>
                            </div>
                        </div>
                        <div id='cssmenu'>
                            <ul>
                                <li><a href='index.html'><span>Home</span></a></li>
                                <li class='has-sub'><a href='#'><span>Profile</span></a>
                                    <ul>
                                        <li class='last'><a href='researchi.html'><span>Research Interests</span></a></li> 
                                        <li class='last'><a href='awards.html'><span>Awards & Recoginitions </span></a></li>
                                        <li class='last'><a href='memberships.html'><span>Memberships</span></a></li>            


                                    </ul>
                                </li>
                                <li><a href='research.html'><span>Research</span></a></li>
                                <li class='has-sub'><a href='#'><span>Contributions</span></a>
                                    <ul>
                                        <li class='last'><a href='publications.html'><span>Publications</span></a></li>
                                        <li class='last'><a href='presentations.html'><span>Presentations</span></a></li>              
                                        <li><a href='media.html'><span>Media</span></a></li>

                                    </ul>
                                </li>
                                <li class='has-sub'><a href='#'><span>Activities</span></a>
                                    <ul>
                                        <li class='last'><a href='supervisory.html'><span>Supervisory Activities</span></a></li>
                                        <li class='last'><a href='administrative.html'><span>Administrative Activities</span></a></li>              


                                    </ul>
                                </li>
                                <li><a href='teaching.html'><span>Work Experience</span></a></li>
                                <li><a href='education.html'><span>Education</span></a></li>


                                <li class='last'><a href='contact.html'><span>Contact</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="main">
					<center><img src="img/underConstructor_8.jpg" width="300" height="250"/></center>
                </div> 
            </div>  
        </div>
        <div id="socialside">
        </div>
    </body>
</html>