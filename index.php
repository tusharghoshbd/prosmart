<!DOCTYPE html>
<html lang="en">    
    <head>
        <title>Professor Mohamed Cheriet personal page</title>
        
        <?php include_once 'layout/header.php'; ?> 


        <script type="text/javascript">
            $(document).ready(function () {

                if ( $(window).width() <= 767) { 
                  $(window).scroll(function () {
                        if ( $(this).scrollTop() > 323) {
                            $(".mobile-menu").addClass("fixed");
                        }
                        else {
                             $(".mobile-menu").removeClass("fixed");
                        }
                    });
                } 
                else {
                  //Add your javascript for small screens here 
                }

                $('.nav-icon').click(function(){
                    $("#example-navbar-collapse").slideToggle();
                });

                 
                lineChart();



            });// end of documetn


            function lineChart(){
                $("#container").highcharts({

                    title: {
                        text: 'Annual Publications by Year'
                    },

              

                    yAxis: {
                        title: {
                            text: 'Number of Publications'
                        }
                    },
                
                    xAxis: {
                        categories: [2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017]
                    },

                  

                    series: [{
                        name: 'ISI journal',
                        data: [0, 28, 25, 20, 28, 28, 47, 79,72,45,118,190,198, 115, 0]
                    }, {
                        name: 'SCOPUS',
                        data: [0, 25, 141, 164, 130, 255, 240, 230,242,256,239,230,120,149,58]
                    }, {
                        name: 'Other Indexing',
                        data: [25,60, 40,25,160,180,176, 100,262,241,205,170,110,58,0]
                    }, {
                        name: 'Book',
                        data: [0, 0, 10, 19, 11, 2,22,2,9,6,1,5,17, 0, 27]
                    }, {
                        name: 'Chapter in Book',
                        data: [8, 8, 5, 18, 9, 16, 4, 1,0,2,41,5,9,7,2]
                    }]

                });//end of chart container
            }



        </script>
    </head>
    <body>
        <div class="container-fluid"><!-- 
            <a href="#sidebar" class="mobilemenu"><i class="icon-reorder"></i></a> -->
            <div class="row no-padding no-margin">
                <div class="col-md-3 col-sm-3 col-xs-12 no-margin no-padding">
                    <div id="main-nav">
                        <?php include_once 'layout/menu.php'; ?> 
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12 no-margin no-padding">
                    <div class="pageheader">
                        <div class="headercontent">
                            <div class="section-container">
                                
                                <h2 class="title">DASHBOARD</h2>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                  <p></p>                                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class=" section-container headercontent ">
                     <!--    <marquee style=" margin-bottom:5px; margin-top:5px; direction="left" scrollamount="2" bgcolor="#00cccc"loop="-1" >   <center>  <h4 class><blink>Welcome To Professor Mohamed Cheriet personal Web page</blink> </h4></center> </marquee> -->
                        <div class="section color-1"></div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading widget-h bl-default ">
                                        <span>Publication Statistics</span>
                                    </div>
                                    <div class="panel-body">
                                    <div class="table-responsive">
                                        <div id="container"  style="height: 300px; min-width: 400px"></div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- Start Widget Project -->
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading widget-h bl-default">
                                        <span>Last 4 Projects</span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table project-tbl table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="40%">Grant</th>
                                                    <th width="55%">Progress</th>
                                                    <th width="5%">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <span class="widget-label">
                                                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Title : Quran And Hadith Authentication Systems" class="black-tooltip">RP003A-14HNE</a>
                                                    </span>
                                                </td>
                                                <td>
                                                    <div class="progress progress-striped active" style="height:10px; margin:5px 0 0 0;">
                                                        <div class="progress-bar progress-bar-success" style="width:100%"></div>
                                                    </div>                                                  
                                                </td>
                                                <td><span class="label text-size-small label-success">end</span></td>
                                            </tr>
                                                                            <tr>
                                                <td>
                                                    <span class="widget-label">
                                                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Title : CONTEXT-BASED KEYWORD PATTERN CLUSTER ANALYSIS TECHNIQUE ON TACIT KNOWLEDGE OF MILITARY TACTICS CENTER OF EXPERTISE" class="black-tooltip">CR010-2014</a>
                                                    </span>
                                                </td>
                                                <td>
                                                    <div class="progress progress-striped active" style="height:10px; margin:5px 0 0 0;">
                                                        <div class="progress-bar progress-bar-success" style="width:100%"></div>
                                                    </div>                                                  
                                                </td>
                                                <td><span class="label text-size-small label-success">end</span></td>
                                            </tr>
                                                                            <tr>
                                                <td>
                                                    <span class="widget-label">
                                                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Title : Big Data And Mobile Cloud For Collaborative Experiments" class="black-tooltip">RP012C-13AFR</a>
                                                    </span>
                                                </td>
                                                <td>
                                                    <div class="progress progress-striped active" style="height:10px; margin:5px 0 0 0;">
                                                        <div class="progress-bar progress-bar-success" style="width:100%"></div>
                                                    </div>                                                  
                                                </td>
                                                <td><span class="label text-size-small label-success">end</span></td>
                                            </tr>
                                                                            <tr>
                                                <td>
                                                    <span class="widget-label">
                                                        <a href="javascript:void(0)" data-toggle="tooltip" data-placement="left" title="Title : Computational History - Recreating The Past of Kuala Terengganu Port" class="black-tooltip">CG025-2013</a>
                                                    </span>
                                                </td>
                                                <td>
                                                    <div class="progress progress-striped active" style="height:10px; margin:5px 0 0 0;">
                                                        <div class="progress-bar progress-bar-success" style="width:100%"></div>
                                                    </div>                                                  
                                                </td>
                                                <td><span class="label text-size-small label-success">end</span></td>
                                            </tr>
                                        </tbody>
                                        </table>
                                        </div>
                                    </div><!-- / widget content -->
                                     <div class="panel-footer text-center text-size-small f-w-500">
                                        <small>This information is generated from Research Management System</small>
                                    </div>
                                </div><!-- / .panel -->
                            </div>  
                            <!-- End Widget Project -->
                            
                            <!-- Start Widget Collaborator -->
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading widget-h bl-default">
                                        <span>Collaborator</span>
                                    </div>
                                    <div class="panel-body p-b-10 p-t-10 text-center">
                                        
                                        <ul class="users-list clearfix">                                <li>
                                                <a class="users-list-name" target="_blank" href="rohanamahmud">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=rohanamahmud">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="rohanamahmud">rohanamahmud</a>                                             
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="wat">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=wat">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="wat">wat</a>                                               
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="misslaiha">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=misslaiha">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="misslaiha">misslaiha</a>                                               
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="fariza">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=fariza">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="fariza">fariza</a>                                             
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="fazmidar">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=fazmidar">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="fazmidar">fazmidar</a>                                             
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="norisma">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=norisma">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="norisma">norisma</a>                                               
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="maizatul">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=maizatul">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="maizatul">maizatul</a>                                             
                                            </li>
                                                                        <li>
                                                <a class="users-list-name" target="_blank" href="sitihafizah">
                                                <!--<a class="users-list-name" target="_blank" href="cv_dashboard_expert.php?viewid=sitihafizah">-->
                                                <img src="img/users.jpg" class="img-circle img-thumbnail img-thumbnail-avatar" style="width:80px; height:75px;">
                                                </a>
                                                <a class="users-list-name" href="sitihafizah">sitihafizah</a>                                               
                                            </li>
                                        </ul>                           
                                    </div><!-- / widget content -->
                                    <div class="panel-footer text-center text-size-small f-w-500">
                                        <a href="cv_dashboard_collaborator.php?viewid=abdullah"><small>View all collaborators </small></a>                      </div>
                                </div><!-- / .panel -->
                            </div>  
                            <!-- End Widget Collaborator -->
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading widget-h bl-default ">
                                        <h3 class="panel-title">Latest Publication</h3>
                                        <br class="visible-xs" />
                                        <span class="pull-right">
                                            <!-- Tabs -->
                                            <ul class="nav panel-tabs">
                                                <li class="active"><a href="#tab1" data-toggle="tab">Tab 1</a></li>
                                                <li><a href="#tab2" data-toggle="tab">Tab 2</a></li>
                                                <li><a href="#tab3" data-toggle="tab">Tab 3</a></li>
                                                <li><a href="#tab4" data-toggle="tab">Tab 4</a></li>
                                            </ul>
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab1">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At</div>
                                            <div class="tab-pane" id="tab2">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</div>
                                            <div class="tab-pane" id="tab3">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</div>
                                            <div class="tab-pane" id="tab4">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.

                                                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet,</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
            
                            <!-- Start Widget Awards -->
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="padding-top: 5px;">
                                <div class="thumbnail text-center m-b-25">
                                    <div class="panel-body widget-awards">
                                        <div class="widget-top">
                                            <span class="widget-title">Latest Award</span>
                                        </div>
                                        <div class="circle-container">
                                            <!-- Award icon -->
                                            <div id="awardstat" class="svg-container"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 194 186" class="circliful">undefined<text text-anchor="middle" x="100" y="125" style="null" fill="#666"></text><circle cx="100" cy="100" r="57" class="border" fill="none" stroke="#eee" stroke-width="5" stroke-dasharray="360" transform="rotate(-90,100,100)"></circle><circle class="circle" cx="100" cy="100" r="57" fill="none" stroke="#02AFA5" stroke-width="5" stroke-dasharray="270, 20000" transform="rotate(-90,100,100)"></circle><text text-anchor="middle" x="100" y="124" class="icon" style="font-size: 62px" fill="#02AFA5"></text><text class="timer" text-anchor="middle" x="100" y="95" style="font-size: 22px; undefined;" fill="#aaa"></text></svg></div>
                                            <!-- /Award icon -->
                                        </div>
                                        <div class="caption caption-down">
                                            <span class="text-size-small f-w-500">Sijil perkhidmatan cemerlang</span>
                                        </div>
                                                             
                                    </div>                  
                                </div>
                            </div>
                            <!-- End Widget Awards -->
                            
                            <!-- Start Widget Activity -->
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="padding-top: 5px;">
                                <div class="thumbnail text-center m-b-25">
                                    <div class="panel-body widget-activity">
                                        <div class="widget-top">
                                            <span class="widget-title">Latest Evaluation</span>
                                        </div>
                                        <div class="circle-container">
                                            <!-- Activity icon -->
                                            <div id="activitystat" class="svg-container"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 194 186" class="circliful">undefined<text text-anchor="middle" x="100" y="125" style="null" fill="#666"></text><circle cx="100" cy="100" r="57" class="border" fill="none" stroke="#eee" stroke-width="5" stroke-dasharray="360" transform="rotate(-90,100,100)"></circle><circle class="circle" cx="100" cy="100" r="57" fill="none" stroke="#E295AE" stroke-width="5" stroke-dasharray="270, 20000" transform="rotate(-90,100,100)"></circle><text text-anchor="middle" x="100" y="124" class="icon" style="font-size: 62px" fill="#DF94AC"></text><text class="timer" text-anchor="middle" x="100" y="95" style="font-size: 22px; undefined;" fill="#aaa"></text></svg></div>
                                            <!-- /Activity icon -->
                                        </div>
                                                                    <div class="caption caption-down">
                                            <span class="text-size-small f-w-500">e-Nikah Systeme Nikah System</span>
                                        </div>
                                    </div>                  
                                </div>
                            </div>
                            <!-- End Widget Activity -->

                            <!-- Start Widget Awards -->
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" style="padding-top: 5px;">
                                <div class="thumbnail text-center m-b-25">
                                    <div class="panel-body widget-awards">
                                        <div class="widget-top">
                                            <span class="widget-title">Latest Award</span>
                                        </div>
                                        <div class="circle-container">
                                            <!-- Award icon -->
                                            <div id="awardstat" class="svg-container"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 194 186" class="circliful">undefined<text text-anchor="middle" x="100" y="125" style="null" fill="#666"></text><circle cx="100" cy="100" r="57" class="border" fill="none" stroke="#eee" stroke-width="5" stroke-dasharray="360" transform="rotate(-90,100,100)"></circle><circle class="circle" cx="100" cy="100" r="57" fill="none" stroke="#02AFA5" stroke-width="5" stroke-dasharray="270, 20000" transform="rotate(-90,100,100)"></circle><text text-anchor="middle" x="100" y="124" class="icon" style="font-size: 62px" fill="#02AFA5"></text><text class="timer" text-anchor="middle" x="100" y="95" style="font-size: 22px; undefined;" fill="#aaa"></text></svg></div>
                                            <!-- /Award icon -->
                                        </div>
                                        <div class="caption caption-down">
                                            <span class="text-size-small f-w-500">Sijil perkhidmatan cemerlang</span>
                                        </div>
                                                             
                                    </div>                  
                                </div>
                            </div>
                            <!-- End Widget Awards -->
                        </div>
                        <br/><br/>

                       <!--  <ul class="ul-boxed list-unstyled">
                            <li> Full Professor (tenure track 1996, tenure 1998)</li>
                            <li> Holder of Canada Research Chair Tier 1</li>
                            <li>Director, Synchromedia Laboratory and Consortium</li>
                            <li> Director, Green Star Network</li>
                        </ul>
                        <div class="title ">
                            <h3>HOME</h3>
                        </div>

                        <div class="col-md-12 ">
                            <p> Professor Cheriet has published more than 300 technical papers in renown international journals and conferences, and has delivered more than 20 invited talks. In addition, he has authored and published 6 books on pattern recognition, document image analysis and understanding, and computer vision. Among them, the book entitled Character Recognition Systems, a Textbook for Students and Practitioners, is highly acclaimed.</p>
                            <p> Prof. Cheriet is also recognized for his activities in technical journal editorial writing, organizing and taking part in many conferences. He has contributed to the training of more than 70 high qualified personnel. Dr. Cheriet was awarded the Queen Elizabeth II Diamond Jubilee Medal in light of his significant contributions to knowledge improvement in computational intelligence and mathematical modeling for image processing, created by MITCAS to mark the 60th anniversary of Her Majesty’s accession to the throne. He holds NSERC Canada Research Chair Tier 1 in Sustainable Smart echo-Cloud.</p>
                            <p> Dr. Cheriet is a senior member of the IEEE and the chapter founder and former chair of IEEE Montreal Computational Intelligent Systems (CIS).</p>
                        </div>   -->
                    </div>
                </div>
            </div>  
        </div>
    </body>
</html>